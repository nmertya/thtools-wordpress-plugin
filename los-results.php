<style>

</style>
<?php
//Template Name: los-results
//@since   1.0.0

//@package The7\Templates

setlocale(LC_MONETARY, 'en_US.UTF-8');
$email = $_POST["email"];
$c5 = $_POST["c5"];
$c10 = .17;
$c8 = $_POST["c8"];
$c9 = $_POST["c9"];
$c16 = $_POST["c16"];
$c18 = $_POST["c18"];
$c11 = $_POST["c11"];
$c12 = 0.25;
$c13 = 15;
$c14 = 0.50;
$c23 = ($c9*$c10) + ($c8*$c11);
$c24 = ($c5 * $c12);
$c25 =  $c23-$c13; 
$c26 = $c25 / 60;
$c27 = $c24 / $c26;
$c28 = $c27 * $c14;
$c29 = ($c28 * $c10) * $c16;
$c30 = ($c28 * $c10) * $c17;
$c32 = ($c28 * $c11) * $c17;
$c33 = ($c28 * $c11) * $c18;
$c34 = $c32 + $c33;
	
$admitsRev = $c29 + $c30;
$dischargeRev = $c32 + $c33;
$additionalRev = $admitsRev + $dischargeRev;






defined( 'ABSPATH' ) || exit;

$config = presscore_config();
$config->set( 'template', 'page' );



get_header();
?>
<?php include 'header.php'; ?><script> <?php include 'js/main.js'; ?></script> 

 
<?php
if($admitsRev == 0.00){
 echo "<script type='text/javascript'>
 window.location.replace('https://thtoolsstaging.wpengine.com/');
 
 </script>";
}
 
?>
<?php if ( presscore_is_content_visible() ) : ?>

	<div id="content" class="content" role="main" >
		<div class="container-fluid" id="mailBody">
			<div class="row">
			<div class="col-sm-12">
				<div id="results">
						<h2  class="blue">Length of Stay Results</h2>
						<br>
						<h3 id="addrev" class="blue"><?php echo money_format('%n', $additionalRev)?></h3>
						<p>Potential revenue gain from improved overall length of stay.</p>
						<br>
						<h3 id="admrev" class="blue"><?php echo money_format('%n', $admitsRev)?></h3>
						<p>Potential revenue gain from improved average length of stay for admitted 							patients.*
						</p>
						<br>
						<h3 id="disrev" class="blue"><?php echo money_format('%n', $dischargeRev)?></h3>
						<p>Potential revenue gain from improved average length of stay for 									discharged patients.*
						</p>
				</div>
			</div>
		</div>
		<br>
          <!-- Contact -->
        <div class="container contact main">
            <div class="row">
                <div class="col-12">
                    <h3 class="blue">Take the Next Step</h3>
                </div>
        	</div>

            <div class="row">
                <div class="col-sm-12">
                    <p>Find out how our transformative model can improve the clinical, operational and financial performance of your emergency department.</p>
                    <div class="container-fluid buttonGroup">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
								<button class="result_button emailResults" type="button" onclick="emailFunction()">Email my results</button>
                            </div>
                            <div class="col-md-4 col-sm-12">
								<button class="result_button" type="button" onclick="window.print()">Print my results</button>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <a href="/contact/"><button class="result_button" type="button">Contact TeamHealth</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact End -->
        <!-- Footer Start -->
        <div class="container-fluid bottom">
            <br>
           <a href="http://thtoolsstaging.wpengine.com/">Start Over</a><br><br>
           <p class="fine">* These calculations are based on assumptions that may not be accurate for your facility. TeamHealth cannot guarantee these are the results your hospital would realize.</p><br>
		   
        </div>
        <!-- Footer End -->
        <br><br>
		
	</div><!-- #content -->
	
	

	<?php do_action( 'presscore_after_content' ); ?>

<?php endif; ?>

<script>

	function emailFunction(){
		alert("sucess!")
	};
	
	 
</script>