<?php
/*
 * Template Name: THTools-Template
 * Description: A Page Template with a darker design.
 */
 
 get_header();
  

?>
 

<?php include 'header.php'; ?><style><?php include 'css/style.css'; ?></style>
<?php include 'header.php'; ?><script> <?php include 'js/main.js'; ?></script> 

 
<div id="content" class="content" role="main">

		   
        <div id="forms-container">
            <div id="form1" >
				<div id="test1">
					<span class="formHeader" id="form1Header">
						<center>
                        <h3 class="h3">Length of Stay Calculator</h3>
							</center>
                 	</span>
				</div>
                <div id="formBox1">
                    <form action="/los-results/"  method="post" id="lengthOfStay">
                        <br>
						<center>
			
						
                        <label class="space" for="c5">Annual Patient Volume:
							<span class="tooltiptext"><h6>
								Emergency department volume per year 
								</h6></span>
							</label>
						
                        <input class="space" type="text" id="c5" name="c5" size="20" value="25000" required>
                        <br>
							
                        <label class="space" for="c8">LOS-Discharged:
							<span class="tooltiptext"><h6> Average length of stay for emergency department patients from arrival to discharge</h6>
							</span>
						</label>
							
                        <input class="space" type="text" id="c8" name="c8" size="20" value="150" required>
							<br>  


                        <label class="space" for="c9">LOS-Admitted:
							<span class="tooltiptext"><h6> Average length of stay for emergency department patients from arrival to admission in</h6>
							</span>
							</label>
                        <input class="space" type="text" id="c9" name="c9" size="20" value="220" required>
                        <br>


                        <label class="space" for="c16">Average Inpatient Revenue:
							<span class="tooltiptext"><h6>Average revenue per inpatient </h6>
							</span>
							</label>
                        <input class="space" type="text" id="c16" name="c16" size="20" value="5000" required>
                        <br>


                        <label class="space" for="c18">Average Outpatient Revenue:
							<span class="tooltiptext"><h6>Average revenue per outpatient </h6>
							</span>
							</label>
                        <input class="space" type="text" id="c18" name="c18" size="20" value="200" required>
                        <br>


                        <label class="space" for="c11">Admission Rate:
							<span class="tooltiptext"><h6> Emergency department volume per year</h6>
							</span>
							</label>
                        <input class="space" type="text" id="c11" name="c11" size="20" value="17" required>
                        <br>


                        <label class="space" for="email">Your Email: 
							<span class="tooltiptext"><h6> Rest assured, none of your information will be shared or sold</h6>
							</span>
							</label>
                        <input class="space" type="email" id="email" name="email" size="20" required>
                        <br>


                        <label class="space" for="facility">Facility Name: 
							<span class="tooltiptext"><h6> Provide your facility name</h6>
							</span>
							</label>
                        <input class="space" type="text" id="facility" name="facility" size="20" required>
                        <br>
						
                        <label class="state" for="state">State:</label>
                        <select class="state" id="state" name="state" required>
                            <option value="#">-Select-</option>
                            <option value="AL">AL</option>
                            <option value="AK">AK</option>
                            <option value="AZ">AZ</option>
                            <option value="AR">AR</option>
                            <option value="CA">CA</option>
                            <option value="CO">CO</option>
                            <option value="CT">CT</option>
                            <option value="DC">DC</option>
                            <option value="DE">DE</option>
                            <option value="FL">FL</option>
                            <option value="GA">GA</option>
                            <option value="HI">HI</option>
                            <option value="ID">ID</option>
                            <option value="IL">IL</option>
                            <option value="IN">IN</option>
                            <option value="IA">IA</option>
                            <option value="KS">KS</option>
                            <option value="KY">KY</option>
                            <option value="LA">LA</option>
                            <option value="ME">ME</option>
                            <option value="MD">MD</option>
                            <option value="MA">MA</option>
                            <option value="MI">MI</option>
                            <option value="MN">MN</option>
                            <option value="MS">MS</option>
                            <option value="MO">MO</option>
                            <option value="MT">MT</option>
                            <option value="NE">NE</option>
                            <option value="NV">NV</option>
                            <option value="NH">NH</option>
                            <option value="NJ">NJ</option>
                            <option value="NM">NM</option>
                            <option value="NY">NY</option>
                            <option value="NC">NC</option>
                            <option value="ND">ND</option>
                            <option value="OH">OH</option>
                            <option value="OK">OK</option>
                            <option value="OR">OR</option>
                            <option value="PA">PA</option>
                            <option value="RI">RI</option>
                            <option value="SC">SC</option>
                            <option value="SD">SD</option>
                            <option value="TN">TN</option>
                            <option value="TX">TX</option>
                            <option value="UT">UT</option>
                            <option value="VT">VT</option>
                            <option value="VA">VA</option>
                            <option value="WA">WA</option>
                            <option value="WV">WV</option>
                            <option value="WI">WI</option>
                            <option value="WY">WY</option>
                        </select><br><br>
                        <input type="submit" name="losBtn" id="losbutton" onclick="mylosbutton()"
                class="button1" value="Calculate" /> 
                        <input name="button2"
                class="button1" type="reset" value="Reset Values" /> 
						</center> </form> <br>
                </div>
            </div>
            <br>
            <div id="form2">
				<div id="test2">
					<span class="formHeader" id="form2Header">
						<center>
                        <h3 class="h3">Increasing ED Volumes</h3>
							</center>
                 	</span>
				</div>
                <div id="formBox2">
                    <form action="/financialimpact-results/" id="volumes" method="post"><center><br>
                        <label class="space" for="patient">Annual Patient Volume:
						<span class="tooltiptext"><h6> Emergency department volume per year</h6>
							</span>
						</label>
                        <input class="space" type="text" id="volume" name="volume" size="20" value="25000" required><br>
                        


                        <label class="space" for="lpmse">LPMSE %:
						<span class="tooltiptext"><h6>  Percent of patients who leave prior to medical screening </h6>
							</span>
						</label>
                        <input class="space" type="text" id="lpmse" name="lpmse" size="20" value="5"  required><br>
                        


                        <label class="space" for="door">Door to Doctor (D2D):
						<span class="tooltiptext"><h6> Number of minutes for a patient to be seen by a doctor from the time they entered the emergency department</h6>
							</span>
						</label>
                        <input class="space" type="text" id="door" name="d2d" size="20" value="45" required><br>
                        


                        <label class="space" for="email">Your Email:
						<span class="tooltiptext"><h6> Rest assured, none of your information will be shared or sold</h6>
							</span>
						</label>
                        <input class="space" type="email" id="email" name="email" size="20"  required><br>
                        


                        <label class="space" for="facility">Facility Name:
						<span class="tooltiptext"><h6> Provide your facility name</h6>
							</span>
						</label>
                        <input class="space" type="text" id="facility" name="facility" size="20" required><br>
                        

                        <label class="state" for="state">State:</label>
                        <select class="state" id="state" name="state" required>
                            <option value="#">-Select-</option>
                            <option value="AL">AL</option>
                            <option value="AK">AK</option>
                            <option value="AZ">AZ</option>
                            <option value="AR">AR</option>
                            <option value="CA">CA</option>
                            <option value="CO">CO</option>
                            <option value="CT">CT</option>
                            <option value="DC">DC</option>
                            <option value="DE">DE</option>
                            <option value="FL">FL</option>
                            <option value="GA">GA</option>
                            <option value="HI">HI</option>
                            <option value="ID">ID</option>
                            <option value="IL">IL</option>
                            <option value="IN">IN</option>
                            <option value="IA">IA</option>
                            <option value="KS">KS</option>
                            <option value="KY">KY</option>
                            <option value="LA">LA</option>
                            <option value="ME">ME</option>
                            <option value="MD">MD</option>
                            <option value="MA">MA</option>
                            <option value="MI">MI</option>
                            <option value="MN">MN</option>
                            <option value="MS">MS</option>
                            <option value="MO">MO</option>
                            <option value="MT">MT</option>
                            <option value="NE">NE</option>
                            <option value="NV">NV</option>
                            <option value="NH">NH</option>
                            <option value="NJ">NJ</option>
                            <option value="NM">NM</option>
                            <option value="NY">NY</option>
                            <option value="NC">NC</option>
                            <option value="ND">ND</option>
                            <option value="OH">OH</option>
                            <option value="OK">OK</option>
                            <option value="OR">OR</option>
                            <option value="PA">PA</option>
                            <option value="RI">RI</option>
                            <option value="SC">SC</option>
                            <option value="SD">SD</option>
                            <option value="TN">TN</option>
                            <option value="TX">TX</option>
                            <option value="UT">UT</option>
                            <option value="VT">VT</option>
                            <option value="VA">VA</option>
                            <option value="WA">WA</option>
                            <option value="WV">WV</option>
                            <option value="WI">WI</option>
                            <option value="WY">WY</option>
                        </select><br><br>
                        <input type="submit" name="btn2"
                class="button1" value="Calculate" /> 
                        <input name="button2" 
                class="button1" type="reset" value="Reset Values" /> </center> <br>
                    </form>
                </div>
            </div>
            <br>
            <div id="form3">
				<div id="test3">
					<span class="formHeader" id="form3Header">
						<center>
                        <h3 class="h3">EM & HM Integration Calculator</h3>
							</center>
                 	</span>
				</div>
                <div id="formBox3"><center>
                    <form action="/integration-results/" method="post" id="integrate"><br>
                        <label class="space" for="volume">Annual Patient Volume:
						<span class="tooltiptext"><h6> Emergency department volume per year</h6>
							</span>
						</label>
                        <input class="space" type="text" id="volume" name="volume" size="20" value="25000" required><br>
                        

                        <label class="space" for="lpmse">LPMSE %:
						<span class="tooltiptext"><h6>Percent of patients who leave prior to medical screening </h6>
							</span>
						</label>
                        <input class="space" type="text" id="lpmse" name="lpmse" size="20" value="4" required><br>
                        

                        <label class="space" for="rate">Admission Rate:
						<span class="tooltiptext"><h6> Percent of emergency department patients admitted to the hospital</h6>
							</span>
						</label>
                        <input class="space" type="text" id="rate" name="rate" size="20" value="17" required><br>
                        

                        <label class="space" for="admission">Annual Admission:
						<span class="tooltiptext"><h6> Average annual admissions for all payers</h6>
							</span>
						</label>
                        <input class="space" type="text" id="admission" name="admission" size="20" value="5"  required><br>
                        


                        <label class="space" for="alos">ALOS for Inpatients:
						<span class="tooltiptext"><h6> Average length of stay in days</h6>
							</span>
						</label>
                        <input class="space" type="text" id="alos" name="alos" size="20" value="5" required><br>
                        


                        <label class="space" for="cmi">Case Mix Index (CMI):
						<span class="tooltiptext"><h6> Average diagnosis - relates group weight for Medicare volume</h6>
							</span>
						</label>
                        <input class="space" type="text" id="cmi" name="cmi" size="20" value="1.34" required><br>
                        

                        <label class="space" for="penalty">Readmission Penalty:
						<span class="tooltiptext"><h6> Average diagnosis - relates group weight for Medicare volume</h6>
							</span>
						</label>
                        <input class="space" type="text" id="penalty" name="penalty" size="20" value="0.28"  required><br>
                        


                        <label class="space" for="email" align="left">Your Email:
						<span class="tooltiptext"><h6> Rest assured, none of your information will be shared or sold</h6>
							</span>
						</label>
                        <input class="space" type="email" id="email" name="email" size="20" required><br>
                        


                        <label class="space" for="facility">Facility Name:
						<span class="tooltiptext"><h6> Provide your facility name</h6>
							</span>
						</label>
                        <input class="space" type="text" id="facility" name="facility" size="20" required><br>
                        

                        <label class="state" for="state">State:</label>
                        <select class="state" id="state" name="state" required>
                            <option value="#">-Select-</option>
                            <option value="AL">AL</option>
                            <option value="AK">AK</option>
                            <option value="AZ">AZ</option>
                            <option value="AR">AR</option>
                            <option value="CA">CA</option>
                            <option value="CO">CO</option>
                            <option value="CT">CT</option>
                            <option value="DC">DC</option>
                            <option value="DE">DE</option>
                            <option value="FL">FL</option>
                            <option value="GA">GA</option>
                            <option value="HI">HI</option>
                            <option value="ID">ID</option>
                            <option value="IL">IL</option>
                            <option value="IN">IN</option>
                            <option value="IA">IA</option>
                            <option value="KS">KS</option>
                            <option value="KY">KY</option>
                            <option value="LA">LA</option>
                            <option value="ME">ME</option>
                            <option value="MD">MD</option>
                            <option value="MA">MA</option>
                            <option value="MI">MI</option>
                            <option value="MN">MN</option>
                            <option value="MS">MS</option>
                            <option value="MO">MO</option>
                            <option value="MT">MT</option>
                            <option value="NE">NE</option>
                            <option value="NV">NV</option>
                            <option value="NH">NH</option>
                            <option value="NJ">NJ</option>
                            <option value="NM">NM</option>
                            <option value="NY">NY</option>
                            <option value="NC">NC</option>
                            <option value="ND">ND</option>
                            <option value="OH">OH</option>
                            <option value="OK">OK</option>
                            <option value="OR">OR</option>
                            <option value="PA">PA</option>
                            <option value="RI">RI</option>
                            <option value="SC">SC</option>
                            <option value="SD">SD</option>
                            <option value="TN">TN</option>
                            <option value="TX">TX</option>
                            <option value="UT">UT</option>
                            <option value="VT">VT</option>
                            <option value="VA">VA</option>
                            <option value="WA">WA</option>
                            <option value="WV">WV</option>
                            <option value="WI">WI</option>
                            <option value="WY">WY</option>
                        </select><br><br>
						</center>
						<center>
                       <input type="submit" name="btn3"
                class="button1" value="Calculate" /> 
                        <input name="button2" 
                class="button1" type="reset" value="Reset Values" /> </center> <br>
                    </form>
                </div>
				
				
				
            </div>
			<br><br><br>
			
        </div>
		
	 

		
		
    </div>
<?php get_footer(); ?>