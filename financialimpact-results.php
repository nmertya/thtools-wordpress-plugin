<?php
//Template Name: FinancialImpact-Results
//@since   1.0.0

//@package The7\Templates

setlocale(LC_MONETARY, 'en_US.UTF-8');
$lpmse = $_POST["lpmse"];
$d2d = $_POST["d2d"];
$patientVolume = $_POST["volume"];
$losDis = 220;
$losAdm = 150;
$email = $_POST["email"];
$state = $_POST["state"];
$facility = $_POST["facility"];
$message = "Facility: " + $facility + "/n" + "State: " + $state + "/n" + "Your Hosipital's LPMSE%: " + $lpmse + "/n Our LPMSE%: 0.5%/n" + "Your D2D Time: " + $d2d + "/n Our D2D Time: 22min/n" + "Your Total Savings: " + money_format('$%i', $pot_revenue_gain_best_practice_client_performance); 

//Constants used in calculations
$avg_revenue_professional_fees = 400;
$average_ip_revenue = 7500;
$annual_volume_increase = 1.03;

//Percent Variable Calculations
$adm_rate_pct = .17;
$lpmse_pct = $lpmse/100;
$lpmse_adm_rate_at_50pct_of_total = $adm_rate_pct/2;

//Scenario 1 Revenue Calculations
$sc1_num_patients_lpmse_before = $patientVolume * $lpmse_pct;
$sc1_51pct_improvement_in_year_one = $lpmse_pct * .51;
$sc1_num_patients_lpmse_after = $patientVolume * $sc1_51pct_improvement_in_year_one;
$sc1_num_ed_patients_gained = $sc1_num_patients_lpmse_before - $sc1_num_patients_lpmse_after;
$sc1_num_inpatients_gained = $sc1_num_ed_patients_gained * $lpmse_adm_rate_at_50pct_of_total;
$sc1_revenue_from_ed = $sc1_num_ed_patients_gained * $avg_revenue_professional_fees;
$sc1_revenue_from_inpatients = $sc1_num_inpatients_gained * $average_ip_revenue;
$sc1_total = $sc1_revenue_from_ed + $sc1_revenue_from_inpatients;

// Scenario 2 Revenue Calculations
$sc2_num_patients_lpmse_before = $patientVolume * $lpmse_pct;
$sc2_num_patients_lpmse_after = $patientVolume * .005;
$sc2_num_ed_patients_gained = $sc2_num_patients_lpmse_before - $sc2_num_patients_lpmse_after;
$sc2_num_inpatients_gained = $sc2_num_ed_patients_gained * $lpmse_adm_rate_at_50pct_of_total;
$sc2_revenue_from_ed = $sc2_num_ed_patients_gained * $avg_revenue_professional_fees;
$sc2_revenue_from_inpatients = $sc2_num_inpatients_gained * $average_ip_revenue;
$sc2_total = $sc2_revenue_from_ed + $sc2_revenue_from_inpatients;

  // Scenario 3 Revenue Calculations
$sc3_3pct_volume_growth = $patientVolume * $annual_volume_increase;
$sc3_new_ed_patients = $sc3_3pct_volume_growth - $patientVolume;
$sc3_ed_revenue_from_inc_volume = $sc3_new_ed_patients * $avg_revenue_professional_fees;
$sc3_new_inpatients = $sc3_new_ed_patients * $adm_rate_pct;
$sc3_inpatient_revenue_from_inc_volume = $sc3_new_inpatients * $average_ip_revenue;
$sc3_total = $sc3_ed_revenue_from_inc_volume + $sc3_inpatient_revenue_from_inc_volume;

 // Potential Revenue Impact
$pot_revenue_gain_best_practice_client_performance = $sc2_total + $sc3_total;


defined( 'ABSPATH' ) || exit;

$config = presscore_config();
$config->set( 'template', 'page' );

add_action( 'presscore_before_main_container', 'presscore_page_content_controller', 15 );

get_header();
?>
<?php
if($d2d == null){
 echo "<script type='text/javascript'>
  window.location.replace('https://thtoolsstaging.wpengine.com/');
 
 
 </script>";
}
?>

<?php if ( presscore_is_content_visible() ) : ?>
    <div class="main">

        <!-- Logo -->

        <br>
        <!-- Logo End -->

        <!-- Header -->
        <h3 class="blue">Increasing ED Volumes Results</h3>
        <br><br>
        <!-- Header End-->

        <!-- Amount -->
        <h3 class="amount blue"><b><?php echo money_format('%n', $pot_revenue_gain_best_practice_client_performance)?></b></h3><br>
        <p>Potential Revenue gain from increasing emergency department patient volumes by implementing TeamHealth's best
            practices to decrease left prior to medical screening exam (LPMSE) rates.</p><br>
        <!-- Amount End -->

        <!-- Benchmarks -->
        <p><b>Benchmarks</b></p>
        <!-- Benchmarks End -->

        <!-- Table -->
		<!-- Table Mobile -->
		<table id="mobile">
			<thead>
				<th>
					<h4>
						LPMSE%
					</h4>
				</th>
                <th></th>
			</thead>
			<tbody>
				<tr>
					<td>
						<h4>
							Your Hospital
						</h4>
					</td>
					<td>
						<?php echo $lpmse?>
					</td>
				</tr>
				<tr>
					<td>
						<h4>
							National Avg.
						</h4>
					</td>
					<td>
						<p class="data">
                            2%
                        </p>
					</td>
				</tr>
				<tr>
					<td>
						<h4>TeamHealth*</h4>
					</td>
					<td>
						<p class="data">
                            <b>0.5%</b>
                        </p>
					</td>
				</tr>
			</tbody>
			<thead>
				<th>
					<h4>
						D2D Time (minutes)
					</h4>
				</th>
                <th></th>
			</thead>
			<tbody>
				<tr>
					<td>
						<h4>
							Your Hospital
						</h4>
					</td>
					<td id="d2d">
						<?php echo $d2dd?>
					</td>
				</tr>
				<tr>
					<td>
						<h4>
							National Avg.
						</h4>
					</td>
					<td>
						<p class="data">
                           62
                        </p>
					</td>
				</tr>
				<tr>
					<td>
						<h4>TeamHealth*</h4>
					</td>
					<td>
						<p class="data">
                            <b>22</b>
                        </p>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- Table Desktop -->
        <table id="desktop">
            <thead>
                <tr>
                    <th></th>
                    <th>
                        <h4>Your Hospital</h4>
                    </th>
                    <th>
                        <h4>National Average</h4>
                    </th>
                    <th>
                        <h4>TeamHealth*</h4>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <h4>LPMSE%</h4>
                    </td>
                    <td>
                        <p class="data">
                            <?php echo $lpmse?>
                        </p>
                    </td>
                    <td>
                        <p class="data">
                            2%
                        </p>
                    </td>
                    <td>
                        <p class="data">
                            <b>0.5%</b>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>D2D Time (minutes)</h4>
                    </td>
                    <td>
                        <p class="data">
                            <?php echo $d2d?>
                        </p>
                    </td>
                    <td>
                        <p class="data">
                           62
                        </p>
                    </td>
                    <td>
                        <p class="data">
                            <b>22</b>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- Table End -->

        <!-- Chart  -->
        <br>
        <div class="container-fluid charts">
            <div class="row center">
		
                <div class="col-md-6 col-sm-12 text-center">
                    <p><b>LPMSE %</b></p>
                    <div class="ct-chart1"></div>
                </div>
                <div class="col-md-6 col-sm-12 text-center">
                    <p><b>D2D Time (minutes)</b></p>
                    <div class="ct-chart2"></div>
                </div>
            </div>
        </div>
        
        
        <!-- Chart End -->

          <!-- Contact -->
        <div class="container contact main">
            <div class="row">
                <div class="col-12">
                    <h3 class="blue">Take the Next Step</h3>
                </div>
        	</div>

            <div class="row">
                <div class="col-sm-12">
                    <p>Find out how our transformative model can improve the clinical, operational and financial performance of your emergency department.</p>
                    <div class="container-fluid buttonGroup">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
								<button class="result_button emailResults" type="button" onclick="emailFunction()">Email my results</button>
                            </div>
                            <div class="col-md-4 col-sm-12">
								<button class="result_button" type="button" onclick="window.print()">Print my results</button>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <a href="/contact/"><button class="result_button" type="button">Contact TeamHealth</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact End -->
        <!-- Footer Start -->
        <div class="container-fluid bottom">
            <br>
           <a href="http://thtoolsstaging.wpengine.com/">Start Over</a><br><br>
           <p class="fine">* These calculations are based on assumptions that may not be accurate for your facility. TeamHealth cannot guarantee these are the results your hospital would realize.</p><br>
		    
        </div>
        <!-- Footer End -->
        <br><br>
	</div><!-- #content -->
	
	

	<?php do_action( 'presscore_after_content' ); ?>

<?php endif; ?>


<script>
	
		function emailFunction(){
		alert("sucess!")
	};
 //JS for charts
		 var data1 = {
            // A labels array that can contain any sort of values
            labels: ['Your Hospital ', 'National Avg.', 'TeamHealth *'],
            // Our series array that contains series objects or in this case series data arrays
            series: [
                [<?php echo $lpmse?>, 2, 0.5]
            ]
        };
	
		var data2 = {
            // A labels array that can contain any sort of values
            labels: ['Your Hospital ', 'National Avg.', 'TeamHealth *'],
            // Our series array that contains series objects or in this case series data arrays
            series: [
                [<?php echo $d2d?>, 62, 22]
            ]
        };

        // As options we currently only set a static size of 300x200 px. We can also omit this and use aspect ratio containers
        // as you saw in the previous example
        var options = {
            width: 305,
            height: 150
        };

        // Create a new line chart object where as first parameter we pass in a selector
        // that is resolving to our chart container element. The Second parameter
        // is the actual data object. As a third parameter we pass in our custom options.
        new Chartist.Bar('.ct-chart1', data1, options);
        new Chartist.Bar('.ct-chart2', data2, options);
	
//Chart JS End

	function myFunction() {
		window.print();
	};
</script>