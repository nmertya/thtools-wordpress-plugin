<?php
//Template Name: integration-results
//@since   1.0.0

//@package The7\Templates
setlocale(LC_MONETARY, 'en_US.UTF-8');
$lpmse = $_POST["lpmse"];
$volume = $_POST["volume"];
	
	

defined( 'ABSPATH' ) || exit;

$config = presscore_config();
$config->set( 'template', 'page' );

add_action( 'presscore_before_main_container', 'presscore_page_content_controller', 15 );

get_header();
?>

<?php
if($lpmse == null){
 echo "<script type='text/javascript'>
  window.location.replace('https://thtoolsstaging.wpengine.com/');
 
 
 </script>";
}
?>
 
<?php if ( presscore_is_content_visible() ) : ?>
    <div class="container-fluid main">
        <br>
        <div class="row"> <!-- Header -->
            <div class="col-12">
                <h3 class="blue">EM & HM Integration Calculator Results</h3><br><br><br>
            </div>
        </div><!-- Header End-->
        <div class="row"> <!-- Table -->
            <div class="col-12">  
                <table id="mobile"> <!-- Table Mobile -->
                    <thead>
                        <th>
                            <h4>
                                Emergency Medicine
                            </h4>
                        </th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <h4>
                                    TeamHealth First Year Results
                                </h4>
                            </td>
                            <td>
                                <p><?php echo $lpmse?></P>
                                <p class="fine">LPMSE Improvement and ED Volume Growth</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4>
                                    TeamHealth Best Practices Results
                                </h4>
                            </td>
                            <td>
                                <p>
                                    <?php echo $lpmse?>
                                </p>
                                <p class="fine">LPMSE imporvement and ED volume growth</p>
                            </td>
                        </tr>
                    </tbody>
                    <thead>
                        <th>
                            <h4>
                                Hospital Medicine
                            </h4>
                        </th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                            <h4>
                                TeamHealth First Year Results
                            </h4>
                            </td>
                            <td>
                                <p class="data">
                                   <?php echo $lpmse?>
                                </p>
                                <p class="fine">
                                    ALOS reduction and inpatientvolumegrowth
                                </p>
                            </td>
                        </tr>
                        <tr>
                        <td>
                                <h4>
                                    TeamHealth Best Practices Results
                                </h4>
                            </td>
                            <td>
                                <p class="data">
                                    <?php echo $volume?>
                                </p>
                                <p class="fine">
                                    ALOS reduction, inpatient volume growth and AntiMicrobial Stewardship Program
                                </p>
                            </td>
                        </tr>
                    </tbody>
                    <thead>
                        <th>
                            <h4>
                                Integrated Services
                            </h4>
                        </th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                            <h4>
                                TeamHealth First Year Results
                            </h4>
                            </td>
                            <td>
                                <p class="data">
                                    <?php echo $volume?>
                                </p>
                                <p class="fine">
                                    ALOS reduction, inpatient volume growth and elimination of readmission penalty
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4>
                                    TeamHealth Best Practices Results
                                </h4>
                            </td>
                            <td>
                                <p class="data">
                                    <?php echo $volume?>
                                </p>
                                <p class="fine">
                                    Above results, plus elimination of readmission penalty and guaranteed 25 basis point improvement in Value Based Purchasing factor
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table> <!-- end mobile -->
                <table id="desktop"> <!-- desktop -->
                    <thead>
                        <tr>
                            <th></th>
                            <th>
                                <h4>TeamHealth First Year Results</h4>
                                <p>Potential Revenue Gain under typical TeamHealth's first year improvements</p>
                            </th>
                            <th>
                                <h4>TeamHealth Best Practice Results</h4>
                                <p>Potential Revenue Gain under TeamHealth's best practice metrics</p>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <h4>Emergency Medicine</h4>
                            </td>
                            <td>
                                <p> 
                                    <?php echo $lpmse?>
                                </p>
                                <p>LPMSE imporvement and ED volume growth</p>
                            </td>
                            <td>
                                <p>
                                    <?php echo $lpmse?>
                                </p>
                                    LPMSE improvement and ED volume growth
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4>Hospital Medicine</h4>
                            </td>
                            <td>
                                <p class="data">
                                    <?php echo $lpmse?>
                                </p>
                                <p>
                                    ALOS reduction and inpatientvolumegrowth
                                </p>
                            </td>
                            <td>
                                <p class="data">
                                <?php echo $volume?>
                                </p>
                                <p>
                                    ALOS reduction, inpatient volume growth and AntiMicrobial Stewardship Program
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4>Intergrated Services</h4>
                            </td>
                            <td>
                                <p class="data">
                                <?php echo $volume?>
                                </p>
                                <p>
                                    ALOS reduction, inpatient volume growth and elimination of readmission penalty
                                </p>
                            </td>
                            <td>
                                <p class="data">
                                <?php echo $volume?>
                                </p>
                                <p>
                                    Above results, plus elimination of readmission penalty and guaranteed 25 basis point improvement in Value Based Purchasing factor
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table><br> 
		        <p class="fine">* These calculations are based on assumptions that may not be accurate for your facility. TeamHealth cannot guarantee these are the results your hospital would realize.</p><br>
            </div>
        </div>
        
        <!-- Table End -->

          <!-- Contact -->
        <div class="container contact main">
            <div class="row">
                <div class="col-12">
                    <h3 class="blue">Take the Next Step</h3>
                </div>
        	</div>

            <div class="row">
                <div class="col-sm-12">
                    <p>Find out how our transformative model can improve the clinical, operational and financial performance of your emergency department.</p>
                    <div class="container-fluid buttonGroup">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
								<button class="result_button emailResults" type="button" onclick="emailFunction()">Email my results</button>
                            </div>
                            <div class="col-md-4 col-sm-12">
								<button class="result_button" type="button" onclick="window.print()">Print my results</button>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <a href="/contact/"><button class="result_button" type="button">Contact TeamHealth</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact End -->
        <!-- Footer Start -->
        <div class="container-fluid bottom">
            <br>
           <a href="http://thtoolsstaging.wpengine.com/">Start Over</a><br><br>
           <p class="fine">* These calculations are based on assumptions that may not be accurate for your facility. TeamHealth cannot guarantee these are the results your hospital would realize.</p><br>
		   
        </div>
        <!-- Footer End -->
        <br><br>

    </div>
<?php endif; ?>

<script>

	function emailFunction(){
		alert("sucess!")
	};
</script>
